/* Copyright 2020 Josef Adamcik
 * Modification for VIA support and RGB underglow by Jens Bonk-Wiltfang
 * Modification for Vial support by Drew Petersen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// clang-format off

#pragma once

// Vial Support
#define VIAL_KEYBOARD_UID { 0x05, 0xCD, 0x9F, 0x8A, 0xF4, 0xDF, 0xDE, 0xB2 }

//

/* ws2812 RGB LED */
#define WS2812_DI_PIN D3


#ifdef RGB_MATRIX_ENABLE
#define RGBLED_NUM 72 // Number of LEDs
#define DRIVER_LED_TOTAL RGBLED_NUM
#define RGB_MATRIX_KEYPRESSES
#define RGB_MATRIX_LED_COUNT 72
#define RGB_MATRIX_FRAMEBUFFER_EFFECTS
#define RGB_MATRIX_SPLIT {36,36}
#define SPLIT_TRANSPORT_MIRROR


                      //
                      //
        #define RGB_MATRIX_HUE_STEP 8
        #define RGB_MATRIX_SAT_STEP 8
        #define RGB_MATRIX_VAL_STEP 8
        #define RGB_MATRIX_SPD_STEP 10
        #define RGBLIGHT_LIMIT_VAL 130
        #define ENABLE_RGB_MATRIX_BREATHING
        #define ENABLE_RGB_MATRIX_RAINDROPS
        #define ENABLE_RGB_MATRIX_TYPING_HEATMAP
        #define ENABLE_RGB_MATRIX_RIVERFLOW
        #define ENABLE_RGB_MATRIX_DIGITAL_RAIN
        #define ENABLE_RGB_MATRIX_REACTIVE
        #define ENABLE_RGB_MATRIX_REACTIVE_WIDE
        #define ENABLE_RGB_MATRIX_REACTIVE_CROSS
        #define ENABLE_RGB_MATRIX_REACTIVE_NEXUS
        #define ENABLE_RGB_MATRIX_SPLASH
        #define ENABLE_RGB_MATRIX_PIXEL_RAIN
        #define ENABLE_RGB_MATRIX_BAND_VAL
        #define ENABLE_RGB_MATRIX_KEYPRESSES

#endif

#ifdef RGBLIGHT_ENABLE
    #undef RGBLED_NUM

	#define RGBLIGHT_EFFECT_BREATHING
	#define RGBLIGHT_EFFECT_RAINBOW_MOOD
	#define RGBLIGHT_EFFECT_RAINBOW_SWIRL
	#define RGBLIGHT_EFFECT_SNAKE
	#define RGBLIGHT_EFFECT_KNIGHT
	#define RGBLIGHT_EFFECT_CHRISTMAS
	#define RGBLIGHT_EFFECT_STATIC_GRADIENT
	#define RGBLIGHT_EFFECT_RGB_TEST
	#define RGBLIGHT_EFFECT_ALTERNATING
	#define RGBLIGHT_EFFECT_TWINKLE

    #define RGBLED_NUM 70
	//#define RGBLED_SPLIT
	#define RGBLED_SPLIT { 35, 35 } // haven't figured out how to use this yet

	//#define RGBLED_NUM 30
    #define RGBLIGHT_LIMIT_VAL 120
    #define RGBLIGHT_HUE_STEP 10
    #define RGBLIGHT_SAT_STEP 17
    #define RGBLIGHT_VAL_STEP 17
#endif

// The four corners
#define VIAL_UNLOCK_COMBO_ROWS { 0, 5, 3, 8 }
#define VIAL_UNLOCK_COMBO_COLS { 0, 0, 0, 0 }
