/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#define VIAL_KEYBOARD_UID {0x3B, 0x6B, 0xA0, 0x29, 0x80, 0x56, 0xED, 0xD1}
#define VIAL_UNLOCK_COMBO_ROWS {0, 0}
#define VIAL_UNLOCK_COMBO_COLS {0, 1}
#define ENCODERS_PAD_A { B2 }
#define ENCODERS_PAD_B { B4 }
#define DYNAMIC_KEYMAP_LAYER_COUNT 6
//#define TAPPING_TERM 180
//#define AUDIO_PIN B6
//#define AUDIO_PWM_DRIVER PWMD0
//#define AUDIO_PWM_CHANNEL RP2040_PWM_CHANNEL_A
//#define AUDIO_INIT_DELAY
//#define AUDIO_CLICKY
//#ifdef AUDIO_ENABLE
//#    define STARTUP_SONG SONG(USSR_ANTHEM)
//#endif

#define OLED_FONT_H "keyboards/crkbd/keymaps/imadam/glcdfont.c"

#ifdef TRACKPOINT_ENABLE
#define PS2_CLOCK_PIN D0
#define PS2_DATA_PIN  D1
#define PS2_PIO_USE_PIO1
#define PS2_MOUSE_ROTATE 270
#define PS2_MOUSE_X_MULTIPLIER 3
#define PS2_MOUSE_Y_MULTIPLIER 3
#define PS2_MOUSE_USE_2_1_SCALING
#endif
#if defined MH_AUTO_BUTTONS
#define MH_AUTO_BUTTONS_LAYER 4
#define MH_AUTO_BUTTONS_TIMEOUT 750
#define PS2_MOUSE_SCROLL_BTN_MASK (1<<PS2_MOUSE_BTN_MIDDLE) /* Default */
#endif


#undef MOUSEKEY_DELAY
#define MOUSEKEY_DELAY          0
#undef MOUSEKEY_INTERVAL
#define MOUSEKEY_INTERVAL       16
#undef MOUSEKEY_WHEEL_DELAY
#define MOUSEKEY_WHEEL_DELAY    0
#undef MOUSEKEY_MAX_SPEED
#define MOUSEKEY_MAX_SPEED      6
#undef MOUSEKEY_TIME_TO_MAX
#define MOUSEKEY_TIME_TO_MAX    64

//#define USE_MATRIX_I2C
#ifdef KEYBOARD_crkbd_rev1_legacy
#    undef USE_I2C
#    define USE_SERIAL
#endif

/* Select hand configuration */

//#define MASTER_LEFT

//#undef MOUSEKEY_DELAY
//#define MOUSEKEY_DELAY          0
//#undef MOUSEKEY_INTERVAL
//#define MOUSEKEY_INTERVAL       16
//#undef MOUSEKEY_WHEEL_DELAY
//#define MOUSEKEY_WHEEL_DELAY    0
//#undef MOUSEKEY_MAX_SPEED
//#define MOUSEKEY_MAX_SPEED      6
//#undef MOUSEKEY_TIME_TO_MAX
//#define MOUSEKEY_TIME_TO_MAX    64
// #define MASTER_RIGHT
#define EE_HANDS

#ifdef RGBLIGHT_ENABLE
    #define RGBLIGHT_EFFECT_BREATHING
    #define RGBLIGHT_EFFECT_RAINBOW_MOOD
    #define RGBLIGHT_EFFECT_RAINBOW_SWIRL
    #define RGBLIGHT_EFFECT_SNAKE
    #define RGBLIGHT_EFFECT_KNIGHT
    #define RGBLIGHT_EFFECT_CHRISTMAS
    #define RGBLIGHT_EFFECT_STATIC_GRADIENT
    #define RGBLIGHT_EFFECT_RGB_TEST
    #define RGBLIGHT_EFFECT_ALTERNATING
    #define RGBLIGHT_EFFECT_TWINKLE
    #define RGBLIGHT_LIMIT_VAL 120
    #define RGBLIGHT_HUE_STEP 10
    #define RGBLIGHT_SAT_STEP 17
    #define RGBLIGHT_VAL_STEP 17
#endif

#define USE_SERIAL_PD2
#ifdef RGBLIGHT_ENABLE
#    undef RGBLED_NUM
#    define RGBLIGHT_ANIMATIONS
#    define RGBLED_NUM 54
#    undef RGBLED_SPLIT
#    define RGBLED_SPLIT \
        { 27, 27 }
#    define RGBLIGHT_LIMIT_VAL 120
#    define RGBLIGHT_HUE_STEP  10
#    define RGBLIGHT_SAT_STEP  17
#    define RGBLIGHT_VAL_STEP  17
#endif



