
PLATFORM_KEY := chibios
MCU := RP2040
BOARD := QMK_PM2040
BOOTLOADER := rp2040
VIA_ENABLE          = yes
VIAL_ENABLE         = yes
LTO_ENABLE          = yes

SERIAL_DRIVER		?= vendor

RGBLIGHT_ENABLE     = yes
#VIALRGB_ENABLE 		= yes
ENCODER_ENABLE		= yes
ENCODER_MAP_ENABLE  = yes
RGB_MATRIX_ENABLE   = no# Can't have RGBLIGHT and RGB_MATRIX at the same time.
MOUSEKEY_ENABLE     = yes
OLED_ENABLE         = yes
#OLED_DRIVER         = SSD1306
EXTRAKEY_ENABLE     = yes
COMBO_ENABLE        = yes
TRACKPOINT_ENABLE	= no
ifeq ($(strip $(TRACKPOINT_ENABLE)), yes)
	PS2_MOUSE_ENABLE	= yes
	PS2_ENABLE			= yes
	PS2_DRIVER			= vendor
endif

MH_AUTO_BUTTONS = yes

ifeq ($(strip $(MH_AUTO_BUTTONS)), yes)
  MOUSEKEY_ENABLE = yes
  OPT_DEFS += -DMH_AUTO_BUTTONS
endif

#AUDIO_ENABLE		= yes
#AUDIO_DRIVER		= pwm_hardware

QMK_SETTINGS        = yes
